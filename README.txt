*******************************************************
    README.txt for userswitcher.module for Drupal
    by Jeff Robbins ::: jeff /@t\ jjeff /d0t\ com
*******************************************************

This module was inspired by the fast user switching
function in the devel.module. I saw the need to use
this as a post-production technical support tool
rather than a development tool and so I've expanded
that function into its own module.

The userswitcher.module allows users with special
permissions (preferably administrators) to "become"
any user on the system. By visiting admin/user/switch
they can enter a username into the autocomplete box,
submit the form, and they are logged in as that user.

This allows support personnel to see what the user is
seeing and manage the user's account from the user's
perspective without needing to learn or reset the user's
password.

To switch back, the administrator should log out and
log back in as themselves.


INSTALLATION:

Put the module in your modules directory.
Enable it.
Done.